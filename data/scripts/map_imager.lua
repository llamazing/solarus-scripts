--[[map_imager.lua
	version 1.1.1
	14 Sep 2019
	GNU General Public License Version 3
	author: Llamazing
	
	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/
	
	This script reads a map.dat file and generates a scaled image that uses various colors
	for each tile type, creating a composite image from all map layers. The image is saved
	as a .ppm file in the .solarus directory.
	
	Usage:
	local map_imager = require"scripts/map_imager"
	map_imager(map_id) --to generate one image for the specified map_id (string)
		--equivalent to map_imager:export_map(map_id)
	map_imager() --to generate images for ALL maps
		--equivalent to map_imager:export_all()
	map_imager:export_world_map(map_id) --to generate an image of the specified map_id (string) and any maps connected by edge teletransporters
]]


local map_imager = setmetatable({}, {
	__call = function(self, ...)
		local n = select('#', ...)
		if n>0 then
			return self:export_map(...)
		else return self:export_all() end
	end,
})

local tilesets = {} --list of tilesets that have been parsed
local maps = {} --(table, key/value) set of maps that have been parsed, map ids (string) as keys
	--[[map.x (number, integer) - x coordinate of the map in pixels
		map.y (number, integer) - y coordinate of the map in pixels
		map.width (number, positive integer) - width of the map in pixels
		map.height (number, positive integer) - height of the map in pixels
		map.min_layer (number, integer) - the lowest layer of the map that contains a tile
		map.max_layer (number, integer) - the highest layer of the map that contains a tile
		map.default_tileset (string) - tileset id of the tileset to use by default for tiles that don't specify it
		map.links (table, key/value) - set of maps that are linked to via a teletransporter on the edge of this map; map ids (string) as keys
			(table, combo) - indices (number, non-negative integer) are offset values of each teletransporter linked to the given map_id in pixels (in ascending order)
				edge (string) - which map edge the linked teletransporters are placed on ("top", "bottom", "left", "right")
	]]

local world_width, world_height = 0, 0


--## Customizable Settings ##--

--constants
local SCALE_FACTOR = 8 --draw maps at 1/8th actual size
local BORDER_WIDTH = 0 --number of pixels for border, set to false or 0 for no border
local IS_ONLY_EDGE_TELE = true --if true then only draw teletransporters that are located on the edge of the map
local IS_OVERLAP_WARNING = true --if true then prints warning message if overlapping maps are detected in export_world_map()
local IS_DISCONNECT_WARNING = true --if true then prints warning message if a discontinuity is detected in export_world_map()
local LAYER_OPACITY = 255 --bottom layer is opaque, layers above are this opacity (where 0 is transparent and 255 is opaque)
local SAVE_DIR = "map_images" --the root directory to use for exported images inside of the quest write directory

--color to draw tile based on ground property; ignore if not listed
local TILE_COLORS = {
	traversable = {187, 153, 85}, --green
	wall = {148, 100, 28}, --grey
	low_wall = {204, 187, 153}, --light grey
	shallow_water = {0, 119, 187}, --light blue
	deep_water = {0, 51, 221}, --blue
	hole = {0, 0, 0}, --black
	ladder = {85, 68, 68}, --dark grey
	lava = {204, 0, 0}, --red
	ice = {100, 204, 204}, --pale blue
	
	--these are entities, not tiles
	['#teletransporter'] = {255, 255, 0}, --yellow; this is a type of entity, not a tile ground property
	['#custom.prickles_blocking'] = {255, 127, 0}, --orange
		--any custom entity can be added using the pattern "#custom.model_name", where "model_name" is the name of the model
	
	--other things to set the color of
	_BORDER_COLOR = {127, 0, 255},
}

--don't draw these tile patterns
local EXCLUDE_TILES = {
	--junk = true, --example of pattern "junk" to be excluded from all tilesets
	tilesets = {
		--jungle = {["199"] = true} --example of pattern "199" to be excluded from 'jungle' tileset
	}
}

--don't draw any map ids matching this list when using 'export_world_map', supports lua pattern matching
local EXCLUDE_MAPS = {
	"/interiors/", --hide any maps in an "interiors" subdirectory
	--"/.*/", --anything buried under two or more directories
}


--## Local functions ##--

--// Returns true if map_id (string) matches a pattern in EXCLUDE_MAPS list
local function is_excluded(map_id)
	local excluded = false --tentative
	for _,pattern in ipairs(EXCLUDE_MAPS or {}) do
		if map_id:match(pattern) then
			excluded = true
			break
		end
	end
	
	return excluded
end

--// Returns true if the given map_data (table, key/value) overlaps with the given range_data (table, key/value), else returns false
local function check_bounds(map_data, range_data)
	--convenience
	local x1 = map_data.world_x --left-most x position of map
	local x2 = x1 + map_data.width - 1 --right-most x position of map
	local y1 = map_data.world_y --top-most y position of map
	local y2 = y1 + map_data.height - 1 --bottom-most y position of map
	local min_x = range_data.min_x --left-most bounds
	local max_x = range_data.max_x --right-most bounds
	local min_y = range_data.min_y --top-most bounds
	local max_y = range_data.max_y --bottom-most bounds
	
	if ((x1>=min_x and x1<=max_x) or (x2>=min_x and x2<=max_x) or (x1<min_x and x2>max_x))
	 and ((y1>=min_y and y1<=max_y) or (y2>=min_y and y2<=max_y) or (y1<min_y and y2>max_y)) then
		return true --overlap detected
	end
	
	return false --no overlap detected
end

--// Calculates position of map linked by edge teletransporters
local POSITION_CALC = {
	--NOTE: offset = src_offset - dst_offset
	top = function(src, dst, offset)
		return src.world_x + offset, src.world_y - dst.height
	end,
	bottom = function(src, dst, offset)
		return src.world_x + offset, src.world_y + src.height
	end,
	left = function(src, dst, offset)
		return src.world_x - dst.width, src.world_y + offset
	end,
	right = function(src, dst, offset)
		return src.world_x + src.width, src.world_y + offset
	end,
}


--// links the src_map (table) being parsed to another dst_map_id (string) by the given teletransporter (table)
	--the link is only created if the teletransporter is located on the edge of the source map
	--return 1: (string or false) specifying the edge of the source map to create the link
		--possible values: "top", "bottom", "left" or "right"
		--false if the teletransporter is not located on the edge of the source map
	--return 2: (number, integer or nil) for the offset of the teletransporter
		--x position in pixels if "top" or "bottom" edge
		--y position if "left" or "right" edge
		--nil if not on edge
local function create_link(src_map, dst_map_id, teletransporter)
	if src_map.links[dst_map_id] then return end --maps already connected, do nothing
	
	--convenience
	local map_width,map_height = src_map.width,src_map.height
	local tele_x,tele_y = teletransporter.x,teletransporter.y
	local tele_width,tele_height = teletransporter.width,teletransporter.height
	
	--determine if teletransporter is located on map edge
	local map_edge = false --tentative
	local offset
	if tele_y <=0 and tele_y >= -tele_height then --top edge of map
		map_edge = "top"
		offset = tele_x
	elseif tele_y >= map_height-tele_height and tele_y <= map_height then --bottom edge of map
		map_edge = "bottom"
		offset = tele_x
	elseif tele_x <= 0 and tele_x >= -tele_width then --left edge of map
		map_edge = "left"
		offset = tele_y
	elseif tele_x >= map_width-tele_width and tele_x <= map_width then --right edge of map
		map_edge =  "right"
		offset = tele_y
	end
	
	if map_edge then
		if not src_map.links[dst_map_id] then
			src_map.links[dst_map_id] = {edge = map_edge} --create new entry for dst map
		end
		
		table.insert(src_map.links[dst_map_id], offset)
	end
	
	return map_edge
end

--// load tileset .dat file to determine ground property of each tile
local function load_tileset(tileset_id)
	local tileset = {}
	
	local env = {}
	function env.tile_pattern(properties)
		local id = properties.id
		assert(id, "tile pattern without id")
		assert(type(id)=="string", "tile pattern must be a string")
		
		local ground = properties.ground
		assert(ground, "tile pattern without ground")
		
		if TILE_COLORS[ground] --ignore ground properties that don't have a color assigned
		 and not EXCLUDE_TILES[id] --skip pattern if it is present in excluded tiles list
		 and not (EXCLUDE_TILES[tileset_id] and EXCLUDE_TILES[tileset_id][id]) then
			tileset[id] = ground
		end
	end
	
	setmetatable(env, {__index = function() return function() end end})
	
	local chunk = sol.main.load_file("tilesets/"..tileset_id..".dat")
	setfenv(chunk, env)
	chunk()
	
	return tileset
end

--// load map .dat file to get list of tiles used
local function load_map(map_id)
	local map = { id=map_id, tiles = {}, tilesets = {}, links = {} }
	
	local env = setmetatable({}, {__index = function() return function() end end})
	
	--properties stores the size and coordinates for the map and the tileset used
	function env.properties(properties)
		local x = tonumber(properties.x)
		assert(x, "property x must be a number")
		local y = tonumber(properties.y)
		assert(y, "property y must be a number")
		
		local width = tonumber(properties.width)
		assert(width, "property width must be a number")
		local height = tonumber(properties.height)
		assert(height, "property height must be a number")
		
		local min_layer = tonumber(properties.min_layer)
		assert(min_layer, "property max_layer must be a number")
		local max_layer = tonumber(properties.max_layer)
		assert(max_layer, "property max_layer must be a number")
		
		local tileset = properties.tileset
		assert(tileset, "properties without tileset")
		
		--set map properties
		map.x = x
		map.y = y
		map.width = width
		map.height = height
		map.min_layer = min_layer
		map.max_layer = max_layer
		map.default_tileset = tileset
		
		--add default tileset to list of tilesets
		if not map.tilesets[tileset] then map.tilesets[tileset] = true end
	end
	
	--each tile defines a size, coordinates and layer as well as the tile id to use
	function env.tile(properties)
		local pattern = properties.pattern --pattern is the tile id
		assert(pattern, "tile without pattern")
		assert(type(pattern)=="string", "tile pattern must be a string")
		
		local tileset = properties.tileset --only specified if not using map default tileset
		
		local layer = properties.layer
		assert(layer, "tile without layer")
		layer = tonumber(layer)
		assert(layer, "tile layer must be a number")
		
		local x = tonumber(properties.x)
		assert(x, "tile x must be a number")
		local y = tonumber(properties.y)
		assert(y, "tile y must be a number")
		
		local width = tonumber(properties.width)
		assert(width, "tile width must be a number")
		local height = tonumber(properties.height)
		assert(height, "tile height must be a number")
		
		--create tile entry and add to map
		table.insert(map.tiles, {
			pattern = pattern,
			tileset = tileset,
			layer = layer,
			x = x,
			y = y,
			width = width,
			height = height,
		})
		
		--add this tile's tileset to map's list of tilesets
		if tileset and not map.tilesets[tileset] then map.tilesets[tileset] = true end
	end
	
	--also extract teletransporter usage to be able to draw teletransporter locations
	function env.teletransporter(properties)
		local layer = properties.layer
		assert(layer, "teletransporter without layer")
		layer = tonumber(layer)
		assert(layer, "teletransporter layer must be a number")
		
		local x = tonumber(properties.x)
		assert(x, "teletransporter x must be a number")
		local y = tonumber(properties.y)
		assert(y, "teletransporter y must be a number")
		
		local width = tonumber(properties.width)
		assert(width, "teletransporter width must be a number")
		local height = tonumber(properties.height)
		assert(height, "teletransporter height must be a number")
		
		local dst_map_id = properties.destination_map
		assert(type(dst_map_id)=="string", "destination map must be a string")
		
		local teletransporter = {
			pattern = "#teletransporter", --instead of using tile pattern to determine color, tile_colors has a "teletransporter" entry
			layer = layer,
			x = x,
			y = y,
			width = width,
			height = height,
			--dst_map_id = dst_map_id,
		}
		
		local is_edge = create_link(map, dst_map_id, teletransporter)
		
		if is_edge or not IS_ONLY_EDGE_TELE then
			table.insert(map.tiles, teletransporter)
		end
	end
	
	function env.custom_entity(properties)
		local layer = properties.layer
		assert(layer, "custom entity without layer")
		layer = tonumber(layer)
		assert(layer, "custom entity layer must be a number")
		
		local x = tonumber(properties.x)
		assert(x, "custom entity x must be a number")
		local y = tonumber(properties.y)
		assert(y, "custom entity y must be a number")
		
		local width = tonumber(properties.width)
		assert(width, "custom entity width must be a number")
		local height = tonumber(properties.height)
		assert(height, "custom entity height must be a number")
		
		local model = properties.model --may be nil
		if not model then return end --don't care about custom entities without model
		assert(type(model)=="string", "custom entity model type must be nil or string")
		
		table.insert(map.tiles, {
			pattern = "#custom."..model,
			layer = layer,
			x = x,
			y = y,
			width = width,
			height = height,
		})
	end
	
	local chunk, err = sol.main.load_file("maps/"..map_id..".dat")
	setfenv(chunk, env)
	chunk()
	
	--sort link offsets in ascending order
	for dst_map_id,links in pairs(map.links or {}) do table.sort(links) end

	return map
end

--// Generates separate surfaces for each layer of the specified map_id and returns them in an table
	--map_id (string) - map id to use to generate the layer images
	--returns (table, key/value) - table containing surfaces for each layer
		--keys are the layer (number, integer), values are the associated sol.surface
		--key "min_layer" - contains the lowest layer value present in table (number, integer)
		--key "max_layer" - contains the highest layer value present in the table (number, integer)
local function generate_map_layers(map_id)
	--load map info from .dat file if not already loaded
	if not maps[map_id] then maps[map_id] = load_map(map_id) end
	local map = maps[map_id]
	
	--load each tileset used from .dat file if not already loaded
	for tileset_id,_ in pairs(map.tilesets) do
		if not tilesets[tileset_id] then tilesets[tileset_id] = load_tileset(tileset_id) end
	end
	
	
	--## Draw Map Layer Images ##--
	
	local image_width = map.width/SCALE_FACTOR + (BORDER_WIDTH and BORDER_WIDTH*2 or 0)
	local image_height = map.height/SCALE_FACTOR + (BORDER_WIDTH and BORDER_WIDTH*2 or 0)
	local x_offset = BORDER_WIDTH or 0
	local y_offset = BORDER_WIDTH or 0
	
	--create a surface for each layer
	local layers = {min_layer = map.min_layer, max_layer = map.max_layer}
	for layer = map.min_layer, map.max_layer do
		local layer_img = sol.surface.create(image_width, image_height)
		
		--draw border if enabled
		local border_color = TILE_COLORS._BORDER_COLOR
		if BORDER_WIDTH and BORDER_WIDTH>0 and border_color then 
			layer_img:fill_color(border_color, 0, 0, image_width, 1) --top 1px line
			layer_img:fill_color(border_color, 0, 1, 1, image_height-2) --left 1px line
			layer_img:fill_color(border_color, 0, image_height-1, image_width, 1) --bottom 1px line
			layer_img:fill_color(border_color, image_width-1, 1, 1, image_height-2) --right 1px line
		end
		layers[layer] = layer_img
	end
	
	--draw each tile
	for _,tile in ipairs(map.tiles) do
		local tileset = tilesets[tile.tileset or map.default_tileset]
		local ground_type = tileset[tile.pattern] or tile.pattern --if tile pattern not found in tileset then use raw pattern value (for entities like "#teletransporter")
		local tile_color = TILE_COLORS[ground_type]
		
		local layer_img = layers[tile.layer]
		
		--draw the tile as solid color on surface corresponding to the correct layer
		if tile_color and layer_img then --ignore corner and empty tiles
			local new_color = {}
			for n=1,3 do
				new_color[n] = math.min(tile_color[n]+tile.layer*16,255)
			end
			tile_color = new_color
			
			layer_img:fill_color(
				tile_color,
				math.floor(tile.x/SCALE_FACTOR) + x_offset,
				math.floor(tile.y/SCALE_FACTOR) + y_offset,
				math.floor(tile.width/SCALE_FACTOR),
				math.floor(tile.height/SCALE_FACTOR)
			)
		end
	end
	
	return layers
end


--// Combines table of surfaces into a single surface by drawing them bottom to top
	--layers (table, array) - keys are integers, values are sol.surfaces
		--layers.min_layer (number, integer) - the bottom layer
		--layers.max_layer(number, integer) - the top layer
	--returns composite sol.surface
local function combine_layers(layers)
	--convenience
	local min_layer = layers.min_layer
	local max_layer = layers.max_layer
	
	local width,height = layers[min_layer]:get_size()
	local surface = sol.surface.create(width, height)
	
	for i=min_layer,max_layer do
		local layer = layers[i]
		if layer then
			if i>min_layer then layer:set_opacity(LAYER_OPACITY) end
			layer:draw(surface)
		end
	end
	
	return surface
end


--// Creates a .ppm image file from a sol.surface at the specified file path
	--surface (sol.surface) - exports an image from the content of this surface
	--file_name (string) - name of the file to export in the quest write directory under SAVE_DIR
	--NOTE: The exported image will not have any transparency
local function write_ppm(surface, file_name)
	local width, height = surface:get_size()
	local MAX_COLOR = 255 --8 bits, don't change
	
	local header = string.format("P6\n%d %d\n%d\n", width, height, MAX_COLOR)
	local file = sol.file.open(SAVE_DIR.."/"..file_name..".ppm", "wb")
	file:write(header)
	
	local pixels = surface:get_pixels()
	
	local data = {}
	local index = 1
	for i=1,pixels:len(),4 do --pixes stores each pixel as R,G,B,A bytes, only need RGB
		data[index] = pixels:sub(i,i+2) --first 3 bytes of each set of 4 bytes
		index = index + 1
	end
	file:write(table.concat(data))
	
	file:flush()
    file:close()
end


--// Generates single map image (.ppm file) and writes it to .solarus directory in SAVE_DIR (directory structure is NOT preserved)
	--map_id (string) - map id of the map to use to generate an image
function map_imager:export_map(map_id)
	assert(type(map_id)=="string", "Bad argument #1 to 'generate_map' (string expected)")
	assert(sol.main.resource_exists("map", map_id), "Bad argument #1 to 'generate_map', invalid map id: "..map_id)
	
	sol.file.mkdir(SAVE_DIR) --create top directory in the write directory in case it doesn't already exist
	
	local surface = combine_layers(generate_map_layers(map_id)) --draw map image
	local file_name = map_id:match"^.+%/([^%/]+)" --strip out directory info, if exists
	if not file_name then file_name = map_id end --in case map_id does not contain "/"
	write_ppm(surface, file_name)
end


--// Generates images (.ppm files) for all maps and writes them to .solarus directory in SAVE_DIR (preserves directory structure)
function map_imager:export_all()
	local map_list = sol.main.get_resource_ids"map"
	
	--## Create directories in write directory ##--
	
	sol.file.mkdir(SAVE_DIR) --create top directory in the write directory in case it doesn't already exist
	local dir_paths = {}
	local dir_lengths = {}
	
	--make list of directories needed
	for _,map_id in ipairs(map_list) do
		local dir_path = map_id:match"[%w%_%/%s]+%/"
		
		if dir_path then
			local dir_length = dir_path:len()
			if not dir_paths[dir_length] then
				dir_paths[dir_length] = {}
				table.insert(dir_lengths, dir_length)
			end
			table.insert(dir_paths[dir_length], SAVE_DIR.."/"..dir_path)
		end
	end
	table.sort(dir_lengths)
	
	--create directories
	for _,n in ipairs(dir_lengths) do
		for _,path in ipairs(dir_paths[n]) do
			sol.file.mkdir(path)
		end
	end
	
	
	--## Generate Map Images ##--
	
	for _,map_id in ipairs(map_list) do
		local surface = combine_layers(generate_map_layers(map_id))
		write_ppm(surface, map_id)
	end
end

--// Generates an image of multiple maps linked together by edge teletransporters
	--map_id (string) - the starting map, draws this map and any linked maps
	--outputs a ppm image in the quest write directory (in SAVE_DIR) with the prefix "WORLD_" followed by the map id
function map_imager:export_world_map(map_id)
	assert(type(map_id)=="string", "Bad argument #1 to 'generate_world_map' (string expected)")
	assert(sol.main.resource_exists("map", map_id), "Bad argument #1 to 'generate_world_map', invalid map id: "..map_id)
	assert(BORDER_WIDTH==0, "ERROR (map imager): BORDER_WIDTH must be 0 to run 'export_world_map' function")
	
	sol.file.mkdir(SAVE_DIR) --create top directory in the write directory in case it doesn't already exist
	
	local surfaces = {}
	local bounds = {} --for overlap detection
	
	--load and draw first map
	surfaces[map_id] = combine_layers(generate_map_layers(map_id))
	table.insert(surfaces, map_id)
	local map = maps[map_id]
	map.world_x, map.world_y = map.x, map.y
	
	--loop #1: recursive function to load and draw each linked map and their linked maps
	local get_linked_maps
	get_linked_maps = function(map_id)
		local map = maps[map_id]
		for linked_map_id,data in pairs(map.links or {}) do
			if not surfaces[linked_map_id] and not is_excluded(linked_map_id) then --only if haven't drawn this map yet and not on excluded list
				surfaces[linked_map_id] = combine_layers(generate_map_layers(linked_map_id))
				local linked_map = maps[linked_map_id]
				if (linked_map.links or {})[map_id] then
					table.insert(surfaces, linked_map_id)
					get_linked_maps(linked_map_id)
				else print("WARNING (map_imager): map "..linked_map_id.." does not have edge teletransporter to map "..map_id) end
			end
		end
	end
	get_linked_maps(map_id)
	
	--loop #2: now figure out coordinates for each
	local min_x, min_y, max_x, max_y = map.x, map.y, map.x+map.width, map.y+map.height
	for _,src_map_id in ipairs(surfaces) do
		local src_map = maps[src_map_id]
		for dst_map_id,src_links in pairs(src_map.links or {}) do
			local dst_map = maps[dst_map_id] or {}
			local dst_links = (dst_map.links or {})[src_map_id]
			if dst_links then
				local edge = src_links.edge
				local offset = src_links[1] - dst_links[1]
				
				assert(dst_links.edge ~= edge, "ERROR (map_imager): mismatched map edges ("..src_map_id.." - "..src_links.edge..", "..dst_map_id.." - "..dst_links.edge..")")
				assert(src_map.world_x and src_map.world_y, "ERROR (map_imager): maps read out of order")
				
				if not dst_map.world_x then --if world coordinates not yet calculated for this map
					local dst_x,dst_y = POSITION_CALC[edge](src_map, dst_map, offset)
					dst_map.world_x, dst_map.world_y = dst_x, dst_y
					
					--update min/max bounds
					local dst_max_x,dst_max_y = dst_x+dst_map.width, dst_y+dst_map.height
					if dst_x<min_x then min_x = dst_x end
					if dst_y<min_y then min_y = dst_y end
					if dst_max_x>max_x then max_x = dst_max_x end
					if dst_max_y>max_y then max_y = dst_max_y end
					
					--record map bounds for overlap warning
					if IS_OVERLAP_WARNING then
						bounds[dst_map_id] = {
							min_x = dst_x,
							max_x = dst_x + dst_map.width - 1,
							min_y = dst_y,
							max_y = dst_y + dst_map.height - 1,
						}
					end
				elseif IS_DISCONNECT_WARNING then --discontinuity may be possible
					local dst_x,dst_y = POSITION_CALC[edge](src_map, dst_map, offset)
					if dst_map.world_x ~= dst_x or dst_map.world_y ~= dst_y then
						print("WARNING (map_imager): discontinuity detected between map "..src_map_id.." and map "..dst_map_id)
					end
				end
			end
		end
	end
	
	--large surface to draw all maps on
	local width = math.floor((max_x-min_x)/SCALE_FACTOR)
	local height = math.floor((max_y-min_y)/SCALE_FACTOR)
	local world_surface = sol.surface.create(width, height)
	world_surface:fill_color{0,0,0}
	
	--loop #3: draw world image
	for _,src_map_id in ipairs(surfaces) do
		local surface = surfaces[src_map_id]
		local src_map = maps[src_map_id]
		local x = math.floor((src_map.world_x-min_x)/SCALE_FACTOR)
		local y = math.floor((src_map.world_y-min_y)/SCALE_FACTOR)
		surface:draw(world_surface, x, y)
		print(src_map_id, src_map.world_x-min_x, src_map.world_y-min_y)
		
		if IS_OVERLAP_WARNING then
			for range_id,map_range in pairs(bounds) do
				if src_map_id ~= range_id and check_bounds(src_map, map_range) then --don't check whether map overlaps with itself
					print("WARNING (map_imager): "..src_map_id.." overlaps with "..range_id)
				end
			end
		end
	end
	
	--write the ppm image file
	local file_name = map_id:match"^.+%/([^%/]+)" --strip out directory info, if exists
	if not file_name then file_name = map_id end --in case map_id does not contain "/"
	file_name = "WORLD_"..file_name
	write_ppm(world_surface, file_name)
end
	
return map_imager

--[[ Copyright 2018-2019 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
