--[[ color_conv.lua
	version 1.0
	9 Feb 2019
	GNU General Public License Version 3
	author: Llamazing
	
	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/
	
	This script contains various functions for the management of colors:
	* Convert colors between RGB/Lab
	* Calculate perceptual difference between two colors (deltaE)
	* Simple matrix operations used for XYZ conversions

	Source for RGB/XYZ/Lab conversions and deltaE calculations:
	http://www.brucelindbloom.com/index.html?Equations.html
]]

local color_list = require"scripts/lib/color_conv/color_list.dat"

local colors = {}

--Pre-calculated RGB2XYZ matrix for sRGB/D65
local sRGB = {
	{ 0.41245643908969, 0.35757607764391, 0.1804374832664 },
	{ 0.21267285140562, 0.71515215528782, 0.07217499330656 },
	{ 0.019333895582329, 0.1191920258813, 0.95030407853637 },
}

--Pre-calculated XYZ2RGB matrix for sRGB/D65
local sRGB_inv = {
	{ 3.2404541621141, -1.5371385127977, -0.49853140955602 },
	{ -0.96926603050519, 1.8760108454467, 0.04155601753035 },
	{ 0.055643430959115, -0.20402591351675, 1.0572251882232 },
}

--Reference illuminant tristimulus values CIE1931
local illuminants = {
	A = {1.09850, 1, 0.35585},
	B = {0.99072, 1, 0.85223},
	C = {0.98074, 1, 1.18232},
	D50 = {0.96422, 1, 0.82521},
	D55 = {0.95682, 1, 0.92149},
	D65 = {0.95047, 1, 1.08883}, --use this one
	D75 = {0.94972, 1, 1.22638},
	E = {1.00000, 1, 1.00000},
	F2 = {0.99186, 1, 0.67393},
	F7 = {0.95041, 1, 1.08747},
	F11 = {1.00962, 1, 0.64350},
}

local kappa = 24389/27
local epsilon = 216/24389

--// Returns determinant of a 2x2 matrix m (table), e.g. {{1,2},{3,4}}
function colors.determinant_2x2(m)
	assert(type(m)=="table", "Bad argument #1 to 'determinant_2x2' (table expected)")
	assert(#m==2, "Bad argument #1 to 'determinant_2x2' (table must contain 2 values)")
	assert(type(m[1])=="table" and #m[1]==2 and type(m[2])=="table" and #m[2]==2, 
		"Bad argument #1 to 'determinant_2x2', incorrect number of entries, expected format: {{1,2},{3,4}}"
	)
	
	local m_1_1 = tonumber(m[1][1])
	local m_1_2 = tonumber(m[1][2])
	local m_2_1 = tonumber(m[2][1])
	local m_2_2 = tonumber(m[2][2])
	
	assert(m_1_1 and m_1_2 and m_2_1 and m_2_2, "Bad argument #1 to 'determinant_2x2', matrix must contain numbers in the format {{1,2},{3,4}}")
	
	return m_1_1*m_2_2 - m_1_2*m_2_1
end

--// Returns determinant of a 3x3 matrix m (table), e.g. {{1,2,3},{4,5,6},{7,8,9}}
function colors.determinant_3x3(m)
	assert(type(m)=="table", "Bad argument #1 to 'determinant_3x3' (table expected)")
	assert(#m == 3, "Bad argument #1 to 'determinant_3x3' (table must contain 3 values)")
	assert(type(m[1])=="table" and #m[1]==3
		and type(m[2])=="table" and #m[2]==3
		and type(m[3])=="table" and #m[3]==3,
		"Bad argument #1 to 'determinant_3x3', incorrect number of entries, expected format: {{1,2,3},{4,5,6},{7,8,9}}"
	)
	
	local m_1_1 = tonumber(m[1][1])
	local m_1_2 = tonumber(m[1][2])
	local m_1_3 = tonumber(m[1][3])
	local m_2_1 = tonumber(m[2][1])
	local m_2_2 = tonumber(m[2][2])
	local m_2_3 = tonumber(m[2][3])
	local m_3_1 = tonumber(m[3][1])
	local m_3_2 = tonumber(m[3][2])
	local m_3_3 = tonumber(m[3][3])
	
	assert(m_1_1 and m_1_2 and m_1_3
		and m_2_1 and m_2_2 and m_2_3
		and m_3_1 and m_3_2 and m_3_3,
		"Bad argument #1 to 'determinant_3x3', matrix must contain numbers in the format {{1,2,3},{4,5,6},{7,8,9}}"
	)
	
	return m_1_1*(m_3_3*m_2_2-m_3_2*m_2_3) - m_2_1*(m_3_3*m_1_2-m_3_2*m_1_3) + m_3_1*(m_2_3*m_1_2-m_2_2*m_1_3)
end

--// Returns the inverse of the 2x2 matrix m (table), e.g. {{1,2},{3,4}}
	--returns false if the determinant of the input matrix is 0
function colors.matrix_inverse_2x2(m)
	local det = colors.determinant_2x2(m)
	
	if det~=0 then
		return {
			{m[2][2]/det, -1*m[1][2]/det},
			{-1*m[2][1]/det, m[1][1]/det},
		}
	else return false end
end

--// Returns the inverse of the 3x3 matrix m (table), e.g. {{1,2,3},{4,5,6},{7,8,9}}
	--returns false if the determinant of the input matrix is 0
function colors.matrix_inverse_3x3(m)
	local det = colors.determinant_3x3(m)
	
	if det~=0 then
		return {
			{
				m[3][3]*m[2][2]-m[3][2]*m[2][3]/det,
				m[3][2]*m[1][3]-m[3][3]*m[1][2]/det,
				m[2][3]*m[1][2]-m[2][2]*m[1][3]/det,
			},{
				m[3][1]*m[2][3]-m[3][3]*m[2][1]/det,
				m[3][3]*m[1][1]-m[3][1]*m[1][3]/det,
				m[2][1]*m[1][3]-m[2][3]*m[1][1]/det
			},{
				m[3][2]*m[2][1]-m[3][1]*m[2][2]/det,
				m[3][1]*m[1][2]-m[3][2]*m[1][1]/det,
				m[2][2]*m[1][1]-m[2][1]*m[1][2]/det
			},
		}
	else return false end
end

--// Used by XYZ2RGB operation, omit gamma value for sRGB
local function compand(val, gamma)
	val = tonumber(val)
	assert(val, "Bad argument #1 to 'compand' (number expected)")
	gamma = tonumber(gamma)
	assert(gamma~=0, "Bad argument #2 to 'compand', value must not be equal to 0")
	
	if not gamma then --sRGB only
		if val > 0.0031308 then
			val = 1.055*(val^(1/2.4)) - 0.055
		else val = 12.92*val end
	else val = val^(1/gamma) end
	
	return val
end

--// Used by RGB2XYZ operation, omit gamma value for sRGB
local function invCompand(val, gamma)
	val = tonumber(val)
	assert(val, "Bad argument #1 to 'compand' (number expected)")
	gamma = tonumber(gamma)
	assert(gamma~=0, "Bad argument #2 to 'compand', value must not be equal to 0")
	
	if not gamma then --sRGB only
		if val > 0.04045 then
			val = ((val + 0.055)/1.055) ^ 2.4
		else val = val/12.92 end
	else val = val^gamma end
	
	return val
end

--// Convert RGB color to XYZ
	--rgb (table, array) - table with 3 number values ranged 0-255 corresponding to red, green & blue color components
		--note alpha value (if present, 4th table value) is ignored
	--Returns table {X, Y, Z}, where X,Y,Z are each numbers from 0 to 1
function colors.rgb2xyz(rgb)
	assert(type(rgb)=="table", "Bad argument #1 to 'rgb2xyz' (table expected)")
	local r,g,b = tonumber(rgb[1]), tonumber(rgb[2]), tonumber(rgb[3])
	assert(r and g and b, "Bad argument #1 to 'rgb2xyz', table must contain 3 number values")
	--TODO validate range of r,g,b values
	
	r = invCompand(r/255)
	g = invCompand(g/255)
	b = invCompand(b/255)
	
	local xyz = {}
	for i,v in ipairs(sRGB) do
		xyz[i] = v[1]*r + v[2]*g + v[3]*b
	end
	
	return xyz
end

--// Convert XYZ color values to CIE-Lab
	--xyz is a table array containing 3 numbers in range 0-1, e.g. {x, y, z}
	--returns table {L, a, b}
function colors.xyz2lab(xyz)
	assert(type(xyz)=="table", "Bad argument #1 to 'xyz2lab' (table expected)")
	
	local f = {}
	for i=1,3 do
		local v = tonumber(xyz[i])
		assert(v, "Bad argument #1 to 'xyz2lab', table value #"..i.." must be a number")
		
		local vref = v/illuminants.D65[i]
		
		if vref > epsilon then
			vref = vref ^ (1/3)
		else vref = (kappa*vref + 16)/116 end
		
		table.insert(f, vref)
	end
	
	local lab = {
		116*f[2] - 16,
		500*(f[1]-f[2]),
		200*(f[2]-f[3]),
	}
	
	return lab
end

--// Convert CIE-Lab color values to XYZ
	--Lab is a table array with 3 numbers, e.g. {100, 0, 0} is white
function colors.lab2xyz(Lab)
	assert(type(Lab)=="table", "Bad argument #1 to 'lab2xyz' (table expected)")
	local L,a,b = tonumber(L[1]), tonumber(L[2]), tonumber(L[3])
	assert(L and a and b, "Bad argument #1 to 'lab2xyz', table must contain 3 number values")
	
	local fy = (L+16)/116
	local f = {
		a/500 + fy,
		fy,
		fy - b/200,
	}
	
	local xyz = {}
	for i,v in ipairs(f) do
		local v3 = v*v*v
		if v3 <= epsilon then
			if i==2 then
				xyz[i] = L/kappa --reduction of redundant steps to reduce rounding error
			else xyz[i] = (116*v - 16)/kappa end
		else xyz[i] = v3 end
		
		xyz[i] = xyz[i]*illuminants.D65[i]
	end
	
	return xyz
end

--// Convert XYZ color values to RGB
	--xyz is a table array containing 3 numbers in range 0-1, e.g. {x, y, z}
	--returns table {r, g, b} with numbers in the range 0-255 corresponding to the red, green, and blue color components
function colors.xyz2rgb(xyz)
	local x,y,z
	
	local rgb = {}
	for i,V in ipairs(sRGB_inv) do
		rgb[i] = V[1]*x + V[2]*y + V[3]*b
	end
	
	return {255*compand(rgb[1]), 255*compand(rgb[2]), 255*compand(rgb[3])}
end

function colors.rgb2lab(r,g,b) return colors.xyz2lab(colors.rgb2xyz(r,g,b)) end
function colors.lab2rgb(l,a,b) return colors.xyz2rgb(colors.lab2xyz(L,a,b)) end

--// Computes 3x3 matrix for RGB/XYZ color conversions given the reference white illuminant
	--white (table, array, optional) - chromatic adaption matix containing 3 number values
		--default: uses D65 reference white
function colors.calc_sRGB(white)
	local white = white or illuminants.D65
	
	local xr, yr = 0.64, 0.33
	local xg, yg = 0.30, 0.60
	local xb, yb = 0.15, 0.06
	
	local m = {
		{xr/yr, xg/yg, xb/yb},
		{1.0, 1.0, 1.0},
		{(1-xr-yr)/yr, (1-xg-yg)/yg, (1-xb-yb)/yb},
	}
	
	local m_inv = colors.matrix_inverse_3x3(m)
	
	local s = {}
	for i,row in ipairs(m_inv) do
		local sum = 0
		for n=1,3 do sum = sum + (row[n]*white[n]) end
		s[i] = sum
	end
	
	local xyz = {}
	for i,row in ipairs(m) do
		xyz[i] = {}
		for n,v in ipairs(row) do
			xyz[i][n] = s[n]*v
		end
	end
	
	return xyz
end

--// Returns the perceptual color difference between two Lab colors
--// deltaE1994 and deltaE2000 have better accuracy but are more computationally intense
	--lab1 (table, array) - Lab color with 3 numbers, e.g. {100, 0, 0} is white
	--lab2 (table, array) - Lab color with 3 numbers, e.g. {0, 0, 0} is black
	--returns positive number 0-100 corresponding to the perceptual color difference (smaller numbers are a closer match)
function colors.deltaE1976(lab1, lab2)
	assert(type(lab1)=="table", "Bad argument #1 to 'deltaE1976' (table expected)")
	assert(type(lab2)=="table", "Bad argument #2 to 'deltaE1976' (table expected)")
	
	--TODO assert table values are 3 numbers each
	
	local dL = lab1[1] - lab2[1]
	local da = lab1[2] - lab2[2]
	local db = lab1[3] - lab2[3]
	
	return math.sqrt(dL*dL + da*da + db*db)
end

--// Returns the perceptual color difference between two Lab colors as a positive number (smaller numbers are a closer match)
--// deltaE2000 has better accuracy but is more computationally intense
	--lab1 (table, array) - Lab color with 3 numbers, e.g. {100, 0, 0} is white
	--lab2 (table, array) - Lab color with 3 numbers, e.g. {0, 0, 0} is black
	--returns positive number 0-100 corresponding to the perceptual color difference (smaller numbers are a closer match)
function colors.deltaE1994(lab1, lab2)
	assert(type(lab1)=="table", "Bad argument #1 to 'deltaE1994' (table expected)")
	assert(type(lab2)=="table", "Bad argument #2 to 'deltaE1994' (table expected)")
	
	local dL = lab1[1] - lab2[1]
	local da = lab1[2] - lab2[2]
	local db = lab1[3] - lab2[3]
	
	local c1 = math.sqrt(lab1[2]*lab1[2] + lab1[3]*lab1[3])
	local c2 = math.sqrt(lab2[2]*lab2[2] + lab2[3]*lab2[3])
	local dC = c1 - c2
	local dH2 = da*da + db*db - dC*dC
	
	local kC = 1 + 0.045*c1
	local kH = 1 + 0.015*c1
	
	return math.sqrt(dL*dL + dC*dC/kC/kC + dH2/kH/kH)
end

--// Returns the perceptual color difference between two Lab colors as a positive number (smaller numbers are a closer match)
	--lab1 (table, array) - Lab color with 3 numbers, e.g. {100, 0, 0} is white
	--lab2 (table, array) - Lab color with 3 numbers, e.g. {0, 0, 0} is black
	--returns positive number 0-100 corresponding to the perceptual color difference (smaller numbers are a closer match)
function colors.deltaE2000(lab1, lab2)
	assert(type(lab1)=="table", "Bad argument #1 to 'deltaE2000' (table expected)")
	assert(type(lab2)=="table", "Bad argument #2 to 'deltaE2000' (table expected)")
	
	local lBarPrime = 0.5*(lab1[1] + lab2[1])
	
	local c1 = math.sqrt(lab1[2]*lab1[2] + lab1[3]*lab1[3])
	local c2 = math.sqrt(lab2[2]*lab2[2] + lab2[3]*lab2[3])
	local cBar = 0.5*(c1+c2)
	local cBar7 = cBar*cBar*cBar*cBar*cBar*cBar*cBar
	
	local g = 0.5*(1 - math.sqrt(cBar7/(cBar7 + 6103515625))) --6103515625 = 25^7
	local a1Prime = lab1[2]*(1+g)
	local a2Prime = lab2[2]*(1+g)
	
	local c1Prime = math.sqrt(a1Prime*a1Prime + lab1[3]*lab1[3])
	local c2Prime = math.sqrt(a2Prime*a2Prime + lab2[3]*lab2[3])
	
	local cBarPrime = 0.5*(c1Prime + c2Prime)
	local cBarPrime7 = cBarPrime * cBarPrime * cBarPrime * cBarPrime * cBarPrime * cBarPrime * cBarPrime
	
	local h1Prime = 180*math.atan2(lab1[3], a1Prime)/math.pi
	local h2Prime = 180*math.atan2(lab2[3], a2Prime)/math.pi
	if h1Prime < 0 then h1Prime = h1Prime + 360 end
	if h2Prime < 0 then h2Prime = h2Prime + 360 end
	local hBarPrime = 0.5*(h1Prime + h2Prime) + (math.abs(h1Prime - h2Prime)>180 and 180 or 0)
	local dTheta = 30*math.exp(-1 * ((hBarPrime-275)/25) * ((hBarPrime-275)/25))
	
	local t = 1 -
		0.17 * math.cos(math.pi * ((hBarPrime - 30)/180) ) +
		0.24 * math.cos(math.pi * (hBarPrime/90) ) +
		0.32 * math.cos(math.pi * ((hBarPrime + 2)/60) ) -
		0.20 * math.cos(math.pi * ((hBarPrime - 15.75)/45) )
	
	local dhPrime = h2Prime - h1Prime + ( math.abs(h2Prime - h1Prime)<=180 and 0 or (h2Prime<=h1Prime and 360 or -360) )
	local dHPrime = 2*math.sqrt(c1Prime*c2Prime) * math.sin(math.pi*(dhPrime/360))
	local dLPrime = lab2[1] - lab1[1]
	local dCPrime = c2Prime - c1Prime
	
	local sL = 1 + 0.015*(lBarPrime - 50)*(lBarPrime - 50)/math.sqrt(20 + (lBarPrime - 50)*(lBarPrime - 50))
	local sC = 1 + 0.045*cBarPrime
	local sH = 1 + 0.015*cBarPrime * t
	
	local rC = math.sqrt(cBarPrime7 / (cBarPrime7 + 6103515625)) --6103515625 = 25^7
	local rT = -2*rC * math.sin(math.pi*(dTheta/90))
	
	return math.sqrt(
		(dLPrime/sL)*(dLPrime/sL) + (dCPrime/sC)*(dCPrime/sC) +
		(dHPrime/sH)*(dHPrime/sH) + (dCPrime/sC)*(dHPrime/sH)*rT
	)
end

--// Calculates deltaE value for 2 rgb colors
	--Input is an RGB color as a string (e.g. "255 255 255"), Lab colors as a table (e.g. {L, a, b}) or as a palette color (1-256) if a number
function colors.deltaE_rgb(rgb1, rgb2)
	local deltaE = colors.deltaE2000 --choose which deltaE function to use here
	
	local lab1 = colors.xyz2lab(colors.rgb2xyz(rgb1))
	local lab2 = colors.xyz2lab(colors.rgb2xyz(rgb2))
	
	return deltaE(lab1, lab2)
end

--// Verifies color values and returns color table
	--value (string or table) - color value to be verified
		--(string) - must be a color name from color_list.dat, returns the color table
		--(table) - contains 3 or 4 entries for red, green, blue and alpha values, range 0 to 255
	--max_count (number, optional) - The maximum number of values in the color table
		--if not specified then up to the first 4 values from the color table will be used
	--returns a table with 3 or 4 entries for red, green, blue and alpha values, range 0 to 255, integer values
		--the returned table will be a copy
		--if value is not valid then returns false along with a string describing the error as the second return value
function colors.RGB_color(value, max_count)
	local color = {}
	
	local max_count = tonumber(max_count)
	assert(not max_count or max_count>0, "Bad argument #2 to 'RGB_color' (number value must be positive)")
	
	if type(value)=="string" then
		local name = value
		value = color_list[value]
		
		if not value then return false, ", invalid color name: "..name end
	end
	
	if type(value)=="table" then
		if not max_count then
			max_count = value[4] and 4 or 3 --exclude alpha if not specified
		elseif max_count > 4 then max_count = 4 end
		
		for i=1,max_count do
			local num_value = tonumber(value[i]) or 0
			num_value = math.min(math.max(math.floor(num_value), 0), 255)
		
			table.insert(color, num_value)
		end
	else return false, " (string or table expected)" end --invalid color format
	
	return color
end

return colors

--[[ Copyright 2016-2019 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
