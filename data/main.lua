require"scripts/multi_events"

--Called when Solarus starts
function sol.main:on_started()
	sol.video.set_window_title("Solarus " .. sol.main.get_solarus_version())
	sol.main.load_settings()
end

--Called when Solarus stops
function sol.main:on_finished()
	sol.main.save_settings()
end

--Called when a keyboard key is pressed.
function sol.main:on_key_pressed(key, modifiers)
	if key == "return" and modifiers.alt then --Alt+Enter to toggle fullscreen
		sol.video.set_fullscreen(not sol.video.is_fullscreen())
		return true
	elseif key == "f4" and modifiers.alt then --Alt+F4 to exit Solarus
		sol.main.exit()
		return true
	end

	return false
end
