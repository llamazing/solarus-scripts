--[[
	This unit test of the multi_events script tests events from metatables of metatables.
]]

local map = ...
local game = map:get_game()

local multi_events = require"scripts/multi_events"
local events_proto = require"tests/events_prototype"

function map:on_opening_transition_finished()
	--Test #1: events from a metatable of a metatable
	local mt_mt1 = {}
	mt_mt1.__index = mt_mt1
	multi_events:enable(mt_mt1)
	
	local mt = {}
	mt.__index = mt
	setmetatable(mt, mt_mt1)
	multi_events:enable(mt)
	
	local obj1 = setmetatable({}, mt)
	multi_events:enable(obj1)
	local function obj1_trigger() obj1:on_activated() end
	events_proto:set_trigger(obj1_trigger)
	
	mt_mt1:register_event("on_activated", function() events_proto:log"mt_mt1" end)
	events_proto:trigger"mt_mt1"
	
	obj1:register_event("on_activated", function() events_proto:log"obj1a" end)
	obj1:register_event("on_activated", function() events_proto:log"obj1b" end)
	events_proto:trigger"mt_mt1;obj1a;obj1b"
	
	
	--Test #2: change the meta metatable
	local mt_mt2 = {}
	mt_mt2.__index = mt_mt2
	multi_events:enable(mt_mt2)
	
	mt_mt2:register_event("on_activated", function() events_proto:log"mt_mt2" end)
	setmetatable(mt, mt_mt2)
	--FAILS events_proto:trigger"mt_mt2;obj1a;obj1b"
	
	
	events_proto:exit()
end
