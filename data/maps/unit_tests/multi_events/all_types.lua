--[[
	This unit test for the multi_events script registers an event for every supported
	Solarus type.
]]

local map = ...
local game = map:get_game()
local hero = game:get_hero()

require"scripts/multi_events"
local events_proto = require"tests/events_prototype"

--Test #1: destination --do this one before map:on_opening_transition_finished()
--TODO does not work, see solarus issue #1450
--[[local destination_cb --call this callback function at map:on_opening_transition_finished()
start:register_event("on_activated", function(self) print"and activated!"
	events_proto:log(self:get_name())
end)
events_proto:set_trigger(function(callback) --delay until map:on_opening_transition_finished()
	destination_cb=callback
	return true
end)
events_proto:trigger"start"]]

function map:on_opening_transition_finished()
	--destination_cb() --conclude Test #1
	
	
	--Test #2: item
	--[[ --TODO replace with item that exists in project
	local item = game:get_item"gem"
	item:register_event("on_pickable_created", function(self, pickable)
		local pickable_name = pickable:get_treasure():get_name()
		local data = {sol.main.get_type(self), self:get_name(), pickable_name}
		events_proto:log(table.concat(data, ","))
	end)
	events_proto:set_trigger(function()
		map:create_pickable{
			x = 88,
			y = 16,
			layer = 1,
			treasure_name = "gem",
			treasure_variant = 1,
		}
	end)
	events_proto:trigger"item,gem,gem"
	]]
	
	--Test #3: surface
	local surface = sol.surface.create()
	--TODO no native events
	
	
	--Test #4: text_surface
	local text_surface = sol.text_surface.create()
	--TODO no native events
	
	
	--Test #5: sprite
	local sprite = sol.sprite.create"hero/tunic1"
	sprite:register_event("on_direction_changed", function(self, animation, direction)
		local data = {sol.main.get_type(self), animation, direction}
		events_proto:log(table.concat(data, ","))
	end)
	events_proto:set_trigger(function()
		sprite:set_direction(1 - sprite:get_direction()) --toggle between direction 1 & 0
	end)
	events_proto:trigger"sprite,stopped,1"
	
	--Test #6: timer
	--TODO no native events
	
	--Test #7: movement
	--TODO ???
	
	--Test #8: straight_movement
	--TODO
	
	--Test #9: target_movement
	--TODO
	
	--Test #10: random_movement
	--TODO
	
	--Test #11: path_movement
	--TODO
	
	--Test #12: random_path_movement
	--TODO
	
	--Test #13: path_finding_movement
	--TODO
	
	--Test #14: circle_movement
	--TODO
	
	--Test #15: jump_movement
	--TODO
	
	--Test #16: pixel_movement
	--TODO
	
	--Test #17: hero
	hero:register_event("on_taking_damage", function(self, damage)
		local data = {sol.main.get_type(self), damage}
		events_proto:log(table.concat(data, ","))
	end)
	events_proto:set_trigger(function()
		hero:start_hurt(1)
	end)
	events_proto:trigger"hero,1"
	
	--Test #18: dynamic_tile
	--TODO no native events, use inherited one
	
	--Test #19: pickable
	--TODO no native events, use inherited one
	
	--Test #20: destructible
	--TODO
	
	--Test #21: carried_object
	--TODO this one is hard, create object and have hero pick it up?
	
	--Test #22: chest
	--TODO use on_opened() with set_open(true) to trigger
	
	--Test #23: shop_treasure
	--TODO this one is hard... could use inherited event
	
	--Test #24: enemy
	--TODO
	
	--Test #25: npc
	--TODO could use on_interaction() when simulating hero movement
	
	--Test #26: block
	--TODO no native events, use inherited one
	
	--Test #27: jumper
	--TODO no native events, use inherited one
	
	--Test #28: switch
	--TODO set_activated() doesn't trigger event
	
	--Test #28: sensor
	--TODO this one is hard... simulate player movements?
	
	--Test #29: separator
	--TODO this one is hard... use inherited event?
	
	--Test #30: wall
	--TODO no native events, use inherited one
	
	--Test #31: crystal
	--TODO no native events, use inherited one
	
	--Test #32: crystal_block
	--TODO no native events, use inherited one
	
	--Test #33: stream
	--TODO no native events, use inherited one
	
	--Test #34: door
	--TODO on_opened() using open() as trigger
	
	--Test #35: stairs
	--TODO no native events, use inherited one
	
	--Test #36: bomb
	--TODO no native events, use inherited one
	
	--Test #37: explosion
	--TODO no native events, use inherited one
	
	--Test #38: fire
	--TODO no native events, use inherited one
	
	--Test #39: arrow
	--TODO no native events, use inherited one
	
	--Test #40: hookshot
	--TODO no native events, use inherited one
	
	--Test #41: boomerang
	--TODO no native events, use inherited one
	
	--Test #42: camera
	--TODO this one is hard... use inherited one?
	
	--Test #43: custom_entity
	--TODO use on_interaction() with simulated hero movement
	
	
	--Test #45: game --do this near end because other tests also pause game
	game:register_event("on_paused", function(self)
		events_proto:log"game"
		self:set_paused(false)
	end)
	events_proto:set_trigger(function()	game:set_paused(true) end)
	events_proto:trigger"game"
	assert(not game:is_paused(), "Test failed: game did not unpause during registered event")
	
	
	--Test #46: teletransporter
	local _,_,layer = hero:get_position()
	local teletransporter = map:create_teletransporter{
		x = 0, y = 0, layer = layer,
		width = 32, height = 32,
		destination_map = map:get_id(),
		enabled_at_start = false,
	}
	teletransporter:register_event("on_activated", function(self)
		self:set_enabled(false)
		events_proto:log(sol.main.get_type(self))
	end)
	events_proto:set_trigger(function(callback)
		--game:simulate_command_pressed"right"
		teletransporter:set_enabled(true)
		
		--have to wait one cycle for teletransporter event to be triggered
		sol.timer.start(map, 10, function() callback() end)
		return true --true means this function will call the callback manually after a delay
	end)
	events_proto:trigger"teletransporter"
	sol.timer.start(map, 10, function() --need small delay before starting next test(s)
	
	
		--[[ --TODO replace with item that exists in project
		--Test #47: map -- do this one last so don't have to wait for slow brandishing animation to finish
		game:get_item"heart":set_brandish_when_picked(false)
		map:register_event("on_obtaining_treasure", function(self, item, variant, savegame_variable)
			local data = {sol.main.get_type(self), item:get_name(), variant, savegame_variable}
			events_proto:log(table.concat(data, ","))
		end)
		events_proto:set_trigger(function()
			hero:start_treasure("heart", 1, "save_name")
		end)
		events_proto:trigger"map,heart,1,save_name"
		]]
		
		
		events_proto:exit()
		--sol.main.exit()
	end)
end
