## Identify Script

(indicate the name of the script(s) in which you are seeking assistance)

## Project Background

(Provide background about your quest project: characters, setting, game mechanics, etc. as it is relevant to the script you are using)

## Goals and Objectives

(Give a description of what you want to accomplish with as much detail as you can)

## Mock-ups and Sample Code

(Provide sample code of what you have tried so far and/or mock-up screenshots detailing what you hope to accomplish)

```
Example Code
```

(also provide any error messages you may have received)

/label ~Help 