## Summary

(Give a short description of what you'd like the newly proposed feature to accomplish)

## Description
### Background - How it is useful

(Provide background information about your quest project and how the proposed feature would come in handy or otherwise describe the situation in which it would be used).

### Implementation & Mock-ups

(give a hypothetical example of how the proposed feature could be implemented, including code excerpts and/or mock-up screenshots as applicable)

```lua
Example code
```

/label ~"Feature Request" 