## Summary

(Summarize the bug encountered concisely)

## Setup
### Solarus Version

(What is your version of Solarus?)

### Script Version

(What is the version of the script that has the problem?)

## Steps to Reproduce

(Describe what to do in order for the issue to occur)

### Example Project

(Do you have an example project that demonstrates the problem? If so then please provide a link)

## Description
### What is the current bug behavior?

(What actually happens)

### What is the expected correct behavior?

(what should happen)

### Example & Screenshots

(Paste any error messages encountered, relevant code excerpts, and/or screenshots)

```
Example error message
```

```lua
Example lua code
```

/label ~Bug 